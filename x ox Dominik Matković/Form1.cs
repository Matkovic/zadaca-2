﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void reset()
        {
            foreach (Button item in Controls.OfType<Button>())
            {
                item.Enabled = true;
                item.BackColor = Color.White;
                Text = "Red's turn";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Text == "Red's turn")
            {
                Text = "Blue's turn";
                ((Button)sender).BackColor = Color.Red;
                ((Button)sender).Enabled = false;
            }
            else
            {
                Text = "Red's turn";
                ((Button)sender).BackColor = Color.Blue;
                ((Button)sender).Enabled = false;
            }

            if (
               (button1.BackColor == Color.Red && button2.BackColor == Color.Red && button3.BackColor == Color.Red)
                ||
               (button4.BackColor == Color.Red && button5.BackColor == Color.Red && button6.BackColor == Color.Red)
                ||
               (button7.BackColor == Color.Red && button8.BackColor == Color.Red && button9.BackColor == Color.Red)
                ||
               (button1.BackColor == Color.Red && button4.BackColor == Color.Red && button7.BackColor == Color.Red)
                ||
               (button2.BackColor == Color.Red && button5.BackColor == Color.Red && button8.BackColor == Color.Red)
                ||
               (button3.BackColor == Color.Red && button6.BackColor == Color.Red && button9.BackColor == Color.Red)
                ||
               (button1.BackColor == Color.Red && button5.BackColor == Color.Red && button9.BackColor == Color.Red)
                ||
               (button3.BackColor == Color.Red && button5.BackColor == Color.Red && button7.BackColor == Color.Red)
                ) { MessageBox.Show("Red wins"); reset(); }

            else if (
               (button1.BackColor == Color.Blue && button2.BackColor == Color.Blue && button3.BackColor == Color.Blue)
                ||
               (button4.BackColor == Color.Blue && button5.BackColor == Color.Blue && button6.BackColor == Color.Blue)
                ||
               (button7.BackColor == Color.Blue && button8.BackColor == Color.Blue && button9.BackColor == Color.Blue)
                ||
               (button1.BackColor == Color.Blue && button4.BackColor == Color.Blue && button7.BackColor == Color.Blue)
                ||
               (button2.BackColor == Color.Blue && button5.BackColor == Color.Blue && button8.BackColor == Color.Blue)
                ||
               (button3.BackColor == Color.Blue && button6.BackColor == Color.Blue && button9.BackColor == Color.Blue)
                ||
               (button1.BackColor == Color.Blue && button5.BackColor == Color.Blue && button9.BackColor == Color.Blue)
                ||
               (button3.BackColor == Color.Blue && button5.BackColor == Color.Blue && button7.BackColor == Color.Blue)
                ) { MessageBox.Show("Blue wins"); reset(); }

            else if (!button1.Enabled && !button2.Enabled && !button3.Enabled && 
                     !button4.Enabled && !button5.Enabled && !button6.Enabled
                  && !button7.Enabled && !button8.Enabled && !button9.Enabled) 
            { MessageBox.Show("Nobody wins"); reset(); }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Text = "Red's turn";
        }
    }
}
